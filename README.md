# First tasks of Rebrain workshop

Perfect project to learn all about devops

## Getting Started

To get started just clone a repository and run `cat nginx.conf` to view current nginx configuration

### Prerequisites

Before using it make sure you have *cat* utility installed

```
$ cat --help
Usage: cat [OPTION]... [FILE]...
Concatenate FILE(s) to standard output.

With no FILE, or when FILE is -, read standard input.

  -A, --show-all           equivalent to -vET
  -b, --number-nonblank    number nonempty output lines, overrides -n
  -e                       equivalent to -vE
  -E, --show-ends          display $ at end of each line
  -n, --number             number all output lines
  -s, --squeeze-blank      suppress repeated empty output lines
  -t                       equivalent to -vT
  -T, --show-tabs          display TAB characters as ^I
  -u                       (ignored)
  -v, --show-nonprinting   use ^ and M- notation, except for LFD and TAB
      --help     display this help and exit
      --version  output version information and exit

Examples:
  cat f - g  Output f's contents, then standard input, then g's contents.
  cat        Copy standard input to standard output.

GNU coreutils online help: <https://www.gnu.org/software/coreutils/>
Full documentation at: <https://www.gnu.org/software/coreutils/cat>
or available locally via: info '(coreutils) cat invocation'
```

### Installing

You shouldn't install anything. Everything will work without installation. Just make sure you cloned the repo

```
$ ls -l
...
drwxrwxr-x 3 user group  4096 июл 31 22:15 rebrain-devops-task1
```

## Running the tests

To run automated scripts write a script which will compare a default nginx configuration in `/etc/nginx/nginx.conf` with configuration in this repository

### And coding style tests

To run these test use gixy from yandex (https://github.com/yandex/gixy). Install it from pip and run it with configuration's path

```
$ pip install gixy
$ gixy /rebrain-devops-task1/nginx.conf
```

## Deployment

Everything works! Just run `cat nginx.conf` to view default nginx configuration

## Built With

* [Nginx](https://nginx.org/ru/docs/) - The web server to run service with default configuration from repo

## Contributing

Please read *CONTRIBUTING.md* for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.rebrainme.com/juliaselut_at_gmail_com/rebrain-devops-task1/-/tags).

## Authors

* **J.S.** - *Initial work* - [FirstProject](https://gitlab.rebrainme.com/juliaselut_at_gmail_com)

See also the list of [contributors](https://gitlab.rebrainme.com/juliaselut_at_gmail_com/rebrain-devops-task1/-/graphs/master) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration

